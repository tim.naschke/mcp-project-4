using Plots
using LaTeXStrings
include("Integrator.jl")

L = 1
K = 210
C = 900
ρ = 2700
Δx = 0.01
Δt = 0.1
Nₜ = 10000

λ = K / (C * ρ)
T₀(x) = sin.(π * x / L)

T = FTCS(T₀(0:Δx:L), Nₜ, λ, Δt, Δx)

plt = plot(
    xlabel=L"x",
    ylabel=L"T",
    title="FTCS",
    dpi=600
)
plot!(0:Δx:L, T₀(0:Δx:L), label=L"t = 0")
plot!(0:Δx:L, T, label=L"t = 1000")
savefig("plots/2a.png")
display(plt)
