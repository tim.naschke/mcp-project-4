using Plots
using LaTeXStrings
include("Integrator.jl")

ε = 0.2
μ = 0.1
Δx = 0.4
Δt = 0.1
Nₓ = 130
Nₜ = 2000
ΔNₜ = 10

x_range = 0:Δx:(Nₓ-1)*Δx
u₀(x) = 1/2 * (1 - tanh((x - 25) / 5))

@time t, results = KdeV(u₀.(x_range), μ, ε, Δx, Δt, Nₜ, ΔNₜ, (1, 0))

anim = @animate for (t, res) in zip(t, results)
    plot(
        res,
        ylim=(0,2),
        title=L"t = %$t",
        label=L"u(x, t)",
        xlabel=L"x",
        xticks=(0:25:125, 0:10:50)
    )
end
gif(anim, "animations/3b.gif", fps=30)
