using Plots
using LaTeXStrings
include("Integrator.jl")

T₀(x) = sin.(π * x / L)

T_exact(x, t) = sin(π * x / L) * exp(- π^2 * K * t / (L^2 * C * ρ))


function ε(t, Δt, simulation_function)
    Nₜ = floor(t / Δt)
    T = simulation_function(T₀(0:Δx:L), Nₜ, λ, Δt, Δx)
    return mean(abs.(T - T_exact.(0:Δx:L, Nₜ * Δt)))
end


L = 1
K = 210
C = 900
ρ = 2700
Δx = 0.01

λ = K / (C * ρ)

Δt_range = LinRange(0.001, 0.7, 1000)

@time @sync let
    @Threads.spawn global ε_ftcs = ε.(100, Δt_range, FTCS)
    @Threads.spawn global ε_eb = ε.(100, Δt_range, EulerBackward)
    @Threads.spawn global ε_cn = ε.(100, Δt_range, CrankNicolson)
    @Threads.spawn global ε_dff = ε.(100, Δt_range, DuFortFrankel)
end

plt = plot(
    xlabel=L"Δt",
    ylabel=L"ε(t = 100)",
    ylim=(0, 0.00005),
    dpi=600
)
plot!(Δt_range, ε_ftcs, label="FTCS")
plot!(Δt_range, ε_eb, label="Euler backward")
plot!(Δt_range, ε_cn, label="Crank-Nicolson")
plot!(Δt_range, ε_dff, label="DuFort-Frankel")

savefig("plots/2c.png")
display(plt)
