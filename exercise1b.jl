using Plots
using LaTeXStrings
include("Integrator.jl")


function solution(x, y, L, terms)
	result = 0
	
	for n in 1:2:2*terms
		result += 1/n * sin(n * π * y / L) * exp(-n * π * x)
	end

	return 400/π * result
end


N = 100
L = 1
terms_list = [1, 10, 100, 1000]
range = 0:L/(N-1):L


results = []
for terms in terms_list
	Φ = zeros(Float64, N, N)

	for i in 1:N, j in 1:N
		Φ[i, j] = solution(range[i], range[j], L, terms)
	end

	hm = heatmap(
		Φ,
		title="#terms = $terms",
		xticks=(20:20:100, 0.2:0.2:1),
		yticks=(20:20:100, 0.2:0.2:1),
		colorbar_title=L"$\Phi$ [V]"
	)
	push!(results, hm)
end

display(plot(
	results...,
	layout=4,
	dpi=600
))
savefig("plots/1b_exact.png")


Φ₀ = zeros(N, N)
Φ₀[1, :] = fill(100, N)
Φ₀[1, 1] = 0
Φ₀[1, N] = 0
b = zeros(N, N)
ε_stop = 10^-3

Φ_sor, _, _, _ = SOR(Φ₀, b, 1.75, ε_stop)
Φ = zeros(Float64, N, N)
for i in 1:N, j in 1:N
	Φ[i, j] = solution(range[i], range[j], L, 1000)
end

diff = @. abs(Φ - Φ_sor)
display(heatmap(
	diff,
	xticks=(20:20:100, 0.2:0.2:1),
	yticks=(20:20:100, 0.2:0.2:1),
	colorbar_title=L"$|\Phi_{\mathrm{exact}} - \Phi_{\mathrm{SOR, 1.75}}|$ [V]",
	dpi=600
))
savefig("plots/1b_difference.png")
