using Plots
using LaTeXStrings
include("Integrator.jl")

N = 100
ε_stop = 10^-3
sor_coefficients = [0.5, 1, 1.25, 1.5, 1.75, 1.99]

Φ = zeros(N, N)
Φ[1, :] = fill(100, N)
Φ[1, 1] = 0
Φ[1, N] = 0
b = zeros(N, N)


res_jac = Jacobi(Φ, b, ε_stop)
res_gs = GaußSeidel(Φ, b, ε_stop)
res_sor = []
for ω in sor_coefficients
    push!(res_sor, SOR(Φ, b, ω, ε_stop))
end


# Plot maximal error
plt1 = plot(
    xlabel="iterations",
    ylabel="maximal error",
    dpi=600
)
plot!(res_jac[2], label="Jacobi")
plot!(res_gs[2], label="Gauß Seidel")
for i in eachindex(res_sor)
    ω = sor_coefficients[i]
    plot!(res_sor[i][2], label=L"SOR, $\alpha=%$ω$")
end


# Plot average error
plt2 = plot(
    xlabel="iterations",
    ylabel="average error",
    dpi=600
)
plot!(res_jac[3], label="Jacobi")
plot!(res_gs[3], label="Gauß Seidel")
for i in eachindex(res_sor)
    ω = sor_coefficients[i]
    plot!(res_sor[i][3], label=L"SOR, $\alpha=%$ω$")
end

plt = plot(plt1, plt2, layout = 2)
savefig("plots/1a_total.png")
display(plt)


# Plot maximal error until 500
cutoff = 500

plt1 = plot(
    xlabel="iterations",
    ylabel="maximal error",
    dpi=600
)
plot!(res_jac[2][1:min(cutoff, end)], label="Jacobi")
plot!(res_gs[2][1:min(cutoff, end)], label="Gauß Seidel")
for i in eachindex(res_sor)
    ω = sor_coefficients[i]
    plot!(res_sor[i][2][1:min(cutoff, end)], label=L"SOR, $\alpha=%$ω$")
end


# Plot average error until 1000
cutoff = 1000

plt2 = plot(
    xlabel="iterations",
    ylabel="average error",
    dpi=600
)
plot!(res_jac[3][1:min(cutoff, end)], label="Jacobi")
plot!(res_gs[3][1:min(cutoff, end)], label="Gauß Seidel")
for i in eachindex(res_sor)
    ω = sor_coefficients[i]
    plot!(res_sor[i][3][1:min(cutoff, end)], label=L"SOR, $\alpha=%$ω$")
end

plt = plot(plt1, plt2, layout = 2)
savefig("plots/1a_cutoff.png")
display(plt)


# Plot maximal error loglog
plt1 = plot(
    xlabel="iterations",
    ylabel="maximal error",
    dpi=600,
    xaxis=:log,
    yaxis=:log,
    legend=false
)
plot!(res_jac[2], label="Jacobi")
plot!(res_gs[2], label="Gauß Seidel")
for i in eachindex(res_sor)
    ω = sor_coefficients[i]
    plot!(res_sor[i][2], label=L"SOR, $\alpha=%$ω$")
end


# Plot average error loglog
plt2 = plot(
    xlabel="iterations",
    ylabel="average error",
    dpi=600,
    xaxis=:log,
    yaxis=:log,
    legend=false
)
plot!(res_jac[3], label="Jacobi")
plot!(res_gs[3], label="Gauß Seidel")
for i in eachindex(res_sor)
    ω = sor_coefficients[i]
    plot!(res_sor[i][3], label=L"SOR, $\alpha=%$ω$")
end

plt = plot(
    plt1, plt2,
    layout = 2,
    legend=:outertop
)
savefig("plots/1a_loglog.png")
display(plt)

println("finished")
