using Plots
using LaTeXStrings
include("Integrator.jl")

ε = 0.2
μ = 0.1
Δx = 0.4
Δt = 0.1
Nₓ = 130
Nₜ = 4000
ΔNₜ = 10

x_range = 0:Δx:(Nₓ-1)*Δx
u₀(x) = 0.8 * (1 - tanh(3x/12 - 3)^2) + 0.3 * (1 - tanh(4.5x/26 - 4.5)^2)

@time t, results = KdeV(u₀.(x_range), μ, ε, Δx, Δt, Nₜ, ΔNₜ, (0, 0))


anim = @animate for (t, res) in zip(t, results)
    plot(
        res,
        ylim=(0,1),
        title=L"t = %$t",
        label=L"u(x, t)",
        xlabel=L"x",
        xticks=(0:25:125, 0:10:50)
    )
end
gif(anim, "animations/3c.gif", fps=30)
