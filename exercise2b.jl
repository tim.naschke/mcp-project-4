using Plots
using LaTeXStrings
include("Integrator.jl")

T₀(x) = sin.(π * x / L)

T_exact(x, t) = sin(π * x / L) * exp(- π^2 * K * t / (L^2 * C * ρ))


function ε(t, Δt)
    Nₜ = floor(t / Δt)
    T = FTCS(T₀(0:Δx:L), Nₜ, λ, Δt, Δx)
    return mean(abs.(T - T_exact.(0:Δx:L, Nₜ * Δt)))
end


L = 1
K = 210
C = 900
ρ = 2700
Δx = 0.01

λ = K / (C * ρ)

Δt_range = LinRange(0.001, 0.7, 1000)

plt1 = plot(
    Δt_range,
    ε.(100, Δt_range),
    xlabel=L"Δt",
    ylabel=L"ε(t = 100)",
    label="FTCS",
    dpi=600
)
plt2 = plot(
    Δt_range,
    ε.(100, Δt_range),
    xlabel=L"Δt",
    ylabel=L"ε(t = 100)",
    label="FTCS",
    ylim=(0, 0.00002),
    dpi=600,
    legend=:topleft
)
plt = plot(plt1, plt2, layout=(2, 1))
savefig("plots/2b.png")
display(plt)


Δt_array = [0.001, 0.1, 0.2, 0.5]
t_range = 0:0.1:100

plt = plot(
    xlabel=L"t",
    ylabel=L"ε(t)",
    dpi=600
)
@time for Δt in Δt_array
    plot!(t_range, ε.(t_range, Δt), label=L"FTCS, $\Delta t = %$Δt$")
end
savefig("plots/2b_time_evolution.png")
display(plt)
