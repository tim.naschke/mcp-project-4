using Plots
using LaTeXStrings
include("Integrator.jl")

N = 100
ε_stop = 10^-3
ω = 2
max_steps = 30000

Φ = zeros(N, N)
Φ[1, :] = fill(100, N)
Φ[1, 1] = 0
Φ[1, N] = 0
b = zeros(N, N)


@time res = SOR(Φ, b, ω, ε_stop, max_steps)
display(plot(
    [res[2], res[3]],
    layout=2,
    ylabel=["maximal error" "average error"],
    xlabel="iterations",
    label=L"SOR, $\alpha = %$ω$",
    dpi=600
))
savefig("plots/1a_bigsor.png")

display(heatmap(
    res[1],
	xticks=(20:20:100, 0.2:0.2:1),
	yticks=(0:20:100, 0:0.2:1),
	colorbar_title=L"$\Phi$ [V]",
    dpi=600
))
savefig("plots/1a_bigsor_heatmap.png")
