using Statistics
using LinearAlgebra


function Jacobi(Φ₀, b, ε_stop)
    Φ = copy(Φ₀)

    ε_max = Any[]
    ε_avrg = Any[]
    steps = 0
    
    while true
        steps += 1

        Φ = JacobiStep(Φ, b)

        εₜ = εrror(Φ)
        push!(ε_max, maximum(εₜ))
        push!(ε_avrg, mean(εₜ))

        if ε_max[end] < ε_stop
            break
        end
    end

    return Φ, ε_max, ε_avrg, steps
end


function JacobiStep(Φ₀, b)
    N = size(Φ₀)[1]
    Φ = copy(Φ₀)

    for i in 2:N-1, j in 2:N-1
        Φ[i, j] = -1/4 * (b[i, j] - Φ₀[i+1, j] - Φ₀[i-1, j] - Φ₀[i, j+1] - Φ₀[i, j-1])
    end

    return Φ
end


function GaußSeidel(Φ₀, b, ε_stop)
    Φ = copy(Φ₀)

    ε_max = Any[]
    ε_avrg = Any[]
    steps = 0
    
    while true
        steps += 1

        GaußSeidelStep!(Φ, b)

        εₜ = εrror(Φ)
        push!(ε_max, maximum(εₜ))
        push!(ε_avrg, mean(εₜ))

        if ε_max[end] < ε_stop
            break
        end
    end

    return Φ, ε_max, ε_avrg, steps
end


function GaußSeidelStep!(Φ, b)
    N = size(Φ)[1]

    for i in 2:N-1
        for j in 2:N-1
            Φ[i, j] = -1/4 * (b[i, j] - Φ[i+1, j] - Φ[i-1, j] - Φ[i, j+1] - Φ[i, j-1])
        end
    end
end


function SOR(Φ₀, b, ω, ε_stop, max_steps=-1)
    Φ = copy(Φ₀)

    ε_max = Any[]
    ε_avrg = Any[]
    steps = 0
    
    while true
        steps += 1

        SORStep!(Φ, b, ω)

        εₜ = εrror(Φ)
        push!(ε_max, maximum(εₜ))
        push!(ε_avrg, mean(εₜ))

        if ε_max[end] < ε_stop || steps >= max_steps > 0
            break
        end
    end

    return Φ, ε_max, ε_avrg, steps
end


function SORStep!(Φ, b, ω)
    N = size(Φ)[1]

    for i in 2:N-1
        for j in 2:N-1
            x_gs = -1/4 * (b[i, j] - Φ[i+1, j] - Φ[i-1, j] - Φ[i, j+1] - Φ[i, j-1])
            Φ[i, j] = ω * x_gs + (1 - ω) * Φ[i, j]
        end
    end
end


function εrror(Φ, i, j)
    return abs(Φ[i, j] - (Φ[i+1, j] + Φ[i-1, j] + Φ[i, j+1] + Φ[i, j-1]) / 4)
end


function εrror(Φ)
    N = size(Φ)[1]
    res = zeros(N, N)

    for i in 2:N-1, j in 2:N-1
        res[i, j] = εrror(Φ, i, j)
    end

    return res
end


function FTCS(T₀, Nₜ, λ, Δt, Δx)
    T = copy(T₀)
    a = λ * Δt / Δx^2
    N = length(T)
    
    M = zeros(N, N)
    M[1, 1] = 1
    M[end, end] = 1
    for diag in N+2:N+1:N*(N-1)-1
        M[diag - 1] = a
        M[diag] = 1 - 2a
        M[diag + 1] = a
    end
    M = M'

    return M^Nₜ * T
end


function EulerBackward(T₀, Nₜ, λ, Δt, Δx)
    T = copy(T₀)
    a = λ * Δt / Δx^2
    N = length(T)
    
    M = Tridiagonal(
        fill(-a, N - 1),
        fill(1 + 2a, N),
        fill(-a, N - 1)
    )

    M[1, 1] = 1
    M[1, 2] = 0
    M[end, end] = 1
    M[end, end-1] = 0

    M = inv(M)

    return M^Nₜ * T
end


function CrankNicolson(T₀, Nₜ, λ, Δt, Δx)
    T = copy(T₀)
    a = λ * Δt / 2Δx^2
    N = length(T)
    
    A = Tridiagonal(
        fill(-a, N - 1),
        fill(1 + 2a, N),
        fill(-a, N - 1)
    )

    A[1, 1] = 1
    A[1, 2] = 0
    A[end, end] = 1
    A[end, end-1] = 0

    A⁻¹ = inv(A)

    B = Tridiagonal(
        fill(a, N - 1),
        fill(1 - 2a, N),
        fill(a, N - 1)
    )

    B[1, 1] = 1
    B[1, 2] = 0
    B[end, end] = 1
    B[end, end-1] = 0

    return (A⁻¹ * B)^Nₜ * T
end


function DuFortFrankel(T₀, Nₜ, λ, Δt, Δx)
    T = copy(T₀)
    T_old = copy(T₀)
    a = 2λ * Δt / Δx^2
    N = length(T)

    for _ in 1:Nₜ
        T_older = T_old
        T_old = T
        T = (1 - a)/(1 + a) * T_older

        for i in 2:N-1
            T[i] += a/(1 + a) * (T_old[i+1] + T_old[i-1])
        end
    end

    return T
end


function KdeV(u₀, μ, ε, Δx, Δt, totalsteps, Δsteps, boundary_values)
    c = ε/3 * Δt/Δx
    d = μ * Δt/Δx^3
    
    u = copy(u₀)
    u[1] = boundary_values[1]
    u[end] = boundary_values[2]
    pushfirst!(u, boundary_values[1])
    push!(u, boundary_values[2])
    
    results = [u[2:end-1]]
    t = [0]
    
    N = length(u)
    u_old = copy(u)

    # first step
    for i in 3:N-2
        u[i] = u_old[i] - c/2 * (u_old[i+1] + u_old[i] + u_old[i-1]) * (u_old[i+1] - u_old[i-1]) - d/2 * (u_old[i+2] + 2 * u_old[i-1] - 2 * u_old[i+1] - u_old[i-2])
    end

    steps = 1

    while steps < totalsteps
        u_older = u_old
        u_old = copy(u)
        u = u_older

        for i in 3:N-2
            u[i] += - c * (u_old[i+1] + u_old[i] + u_old[i-1]) * (u_old[i+1] - u_old[i-1]) - d * (u_old[i+2] + 2 * u_old[i-1] - 2 * u_old[i+1] - u_old[i-2])
        end

        steps += 1
        if steps % Δsteps == 0
            push!(results, u[2:end-1])
            push!(t, steps * Δt)
        end
    end

    return t, results
end
